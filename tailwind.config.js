module.exports = {
    mode: 'jit',
    purge: [
        './public/**/*.html',
        './templates/**/*.html.twig',
    ],
    theme: {
        extend: {}
    },
    variants: {},
    plugins: [
        require('@tailwindcss/forms')
    ],
    prefix: '',
}