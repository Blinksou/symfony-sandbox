# How to install

Update your .env according to your configuration.

````bash

# Install dependencies
composer install

# Update database
php bin/console d:s:u -f

# Load fixtures
php bin:console d:f:l -n

# Install easyadmin assets
php bin/console assets:install

# Build the whole thing 
yarn install && yarn dev

# Enjoy :)
symfony serve
````