const Encore = require('@symfony/webpack-encore');
const WatchExternalFilesPlugin = require('webpack-watch-files-plugin').default;

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .addPlugin(new WatchExternalFilesPlugin({
        files: [
            './templates/**/*.html.twig' // watch symfony twig files
        ],
        verbose: false
    }))
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    // Entries
    .addEntry('app', './assets/app.js')
    .addStyleEntry('tailwind', './assets/styles/tailwind.scss')
    .addStyleEntry('easyadmin', './assets/styles/bundles/easyadmin.scss')

    // Needed
    .enableSassLoader()
    .enablePostCssLoader()

    .enableStimulusBridge('./assets/controllers.json')
    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())

    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })
    .enableIntegrityHashes(Encore.isProduction())
;

module.exports = Encore.getWebpackConfig();
