<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

final class AppFixtures extends Fixture
{
    public function __construct(
        private UserPasswordEncoderInterface $encoder
    )
    {
    }

    public function load(ObjectManager $manager): void
    {
        $admin = new User();

        $admin->setEmail('admin@demo.fr')
            ->setRoles(["ROLE_ADMIN"])
            ->setPassword($this->encoder->encodePassword($admin, 'admin'));

        $manager->persist($admin);

        $manager->flush();
    }
}
